-- queue name
local queue = ARGV[1]
local number = tonumber(ARGV[2]) or 1

assert(queue, 'Missing queue')

local queue_prefix = queue .. ':state:queued:priority:'
local queue_high = queue_prefix .. 'high'
local queue_normal = queue_prefix .. 'normal'
local queue_low = queue_prefix .. 'low'

local packJob = function(id, job)
	local packed = {}

	for current = 1, #job, 2 do
		packed[ job[current] ] = job[current + 1]
	end

	packed.id = id
	packed.backoff = tonumber(packed.backoff)
	packed.timeout = tonumber(packed.timeout)
	packed.attempts = tonumber(packed.attempts)
	packed.max_attempts = tonumber(packed.max_attempts)

	return packed
end

-- fetch all jobs
local jobs = {}
jobs[1] = false
local index = 1
local more_jobs = true

while (index <= number and more_jobs) do
	-- get next job
	local id = redis.call('lpop', queue_high)
	if not id then
		id = redis.call('lpop', queue_normal)
	end
	if not id then
		id = redis.call('lpop', queue_low)
	end

	if id then
		local job_key = queue .. ':job:' .. id
		local timeout_key = job_key .. ':timeout'

		local job = packJob(id, redis.call('hgetall', job_key))

		-- update job stats
		job.status = 'processing'
		job.attempts = job.attempts + 1
		redis.call('hmset', job_key, 'status', 'processing')
		redis.call('hincrby', job_key, 'attempts', 1)

		-- add to processing queue so timeout can be measured
		redis.call('sadd', queue .. ':state:processing', id)
		redis.call('set', timeout_key, 1)
		redis.call('expire', timeout_key, job.timeout)

		-- leave first one as is to coerce it to an array
		jobs[index + 1] = job
	end

	index = index + 1
end

-- json encode jobs so they are returned in the same format that they were added
for i, job in ipairs(jobs) do
	-- skip first one
	if jobs[i] then
		jobs[i] = cjson.encode(job)
	end
end

return jobs