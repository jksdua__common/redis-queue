--[[
	Maintenance tasks:
		- removes jobs that have been completed/failed
		- requeues delayed tasks that have completed their timeout

	@param {String} queue Name of the queue
	@param {Number} timestamp UNIX timestamp
	@param {Number} [completed_records_to_keep=0] Number of records to retain
--]]

-- queue name
local queue = ARGV[1]
-- timestamp
local timestamp = ARGV[2]
-- number of records to keep in the queue
local completed_records_to_keep = tonumber(ARGV[3]) or 0

assert(queue, 'Missing queue')
assert(timestamp, 'Missing timestamp')
assert(completed_records_to_keep, 'Missing record length')

local packJob = function(id, job)
	local packed = {}

	for current = 1, #job, 2 do
		packed[ job[current] ] = job[current + 1]
	end

	packed.id = id
	packed.backoff = tonumber(packed.backoff)
	packed.timeout = tonumber(packed.timeout)
	packed.attempts = tonumber(packed.attempts)
	packed.max_attempts = tonumber(packed.max_attempts)

	return packed
end

local removeCompleted = function()
	local queue_key = queue .. ':state:done'
	local total_records = tonumber(redis.call('llen', queue_key))
	local records_to_delete = 0

	if total_records > 0 then
		records_to_delete = total_records - completed_records_to_keep

		while records_to_delete > 0 do
			-- remove from list
			local id = redis.call('lpop', queue_key)
			-- remove job data
			local job_key = queue .. ':job:' .. id
			redis.call('del', job_key)

			records_to_delete = records_to_delete - 1
		end
	end
end

-- requeue tasks that have timed out
local requeueTimedOut = function()
	local processing_queue_key = queue .. ':state:processing'
	local queue_key_prefix = queue .. ':state:queued:priority:'
	local ids = redis.call('smembers', processing_queue_key)

	for i, id in ipairs(ids) do
		local job_key = queue .. ':job:' .. id
		local timeout_key = job_key .. ':timeout'
		local time_pending = redis.call('get', timeout_key)

		if not time_pending then
			-- get job data
			local job = packJob(id, redis.call('hgetall', job_key))
			local queue_key = queue_key_prefix .. job.priority

			-- push to top of the queue
			redis.call('lpush', queue_key, id)

			-- update job stats
			job.attempts = job.attempts + 1
			redis.call('hincrby', job_key, 'attempts', 1)

			if job.max_attempts > job.attempts then
				redis.call('hmset', job_key, 'status', 'queued')
				redis.call('publish', queue .. ':job:timeout', cjson.encode(job))
			else
				redis.call(
					'hmset', job_key,
					'status', 'failed',
					'failed_at', timestamp,
					'finished_at', timestamp
				)

				-- remove from processing queue
				redis.call('srem', processing_queue_key, id)

				-- publish event so clients can subscribe
				redis.call('publish', queue .. ':job:failed', cjson.encode(job))
			end
		end
	end
end

-- loops through delayed tasks and requeues them if ready
local requeueDelayed = function()
	local delayed_queue_key = queue .. ':state:delayed'
	local queue_key_prefix = queue .. ':state:queued:priority:'
	local ids = redis.call('smembers', delayed_queue_key)

	for i, id in ipairs(ids) do
		local job_key = queue .. ':job:' .. id
		local timeout_key = job_key .. ':timeout'
		local time_pending = redis.call('get', timeout_key)

		if not time_pending then
			-- get job priority
			local priority = redis.call('hmget', job_key, 'priority')
			local queue_key = queue_key_prefix .. priority[1]
			-- push to top of the queue
			redis.call('lpush', queue_key, id)

			-- update job stats
			redis.call('hmset', job_key, 'status', 'queued')

			-- remove from delayed queue
			redis.call('srem', delayed_queue_key, id)
		end
	end
end

removeCompleted()
requeueTimedOut()
requeueDelayed()

return { ok = true }