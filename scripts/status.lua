local STATUSES = { done = 1, failed = 1 }

-- queue name
local queue = ARGV[1]
assert(queue, 'Missing queue')

-- job details
local job = nil
if not pcall(function() job = cjson.decode(ARGV[2]) end) then
	assert(job, 'Invalid job - expected json')
end
assert(job.id, 'Missing job id')
assert(job.status, 'Missing job status')
assert(job.timestamp, 'Missing timestamp')
assert(STATUSES[job.status], 'Invalid job status')

-- assert error exists if job failed
if 'failed' == job.status then
	assert(job.error, 'Missing error - required for failed jobs')
end

local packJob = function(id, job)
	local packed = {}

	for current = 1, #job, 2 do
		packed[ job[current] ] = job[current + 1]
	end

	packed.id = id
	packed.backoff = tonumber(packed.backoff)
	packed.timeout = tonumber(packed.timeout)
	packed.attempts = tonumber(packed.attempts)
	packed.max_attempts = tonumber(packed.max_attempts)

	return packed
end

local job_key = queue .. ':job:' .. job.id
local retrieved = packJob(job.id, redis.call('hgetall', job_key))

-- validate job exists and hasnt already been processed by another worker
assert(retrieved, 'Job not found')
assert(retrieved.status == 'processing', 'Invalid job status `' .. retrieved.status .. '` - cannot process')

-- remove from processing queue
redis.call('srem', queue .. ':state:processing', job.id)
redis.call('del', job_key .. ':timeout')

-- for failed tasks, need to determine if job is marked as failed or marked as to be requeued
if 'failed' == job.status then
	retrieved.error = job.error
	retrieved.failed_at = job.timestamp
	redis.call(
		'hmset', job_key,
		'error', job.error,
		'failed_at', job.timestamp
	)

	local attempts_left = retrieved.max_attempts - retrieved.attempts

	-- if we have more attempts, update status to failed attempt and add a delay timer
	if attempts_left > 0 then
		job.status = 'failed_attempt'
		retrieved.status = 'failed_attempt'

		-- setup delay timer - deletes key once its expired to indicate its ready to go back
			-- timeout is exponential to backoff and number of attempts
		local timeout = retrieved.backoff * retrieved.attempts * retrieved.timeout
		redis.call('set', job_key .. ':timeout', 1)
		redis.call('expire', job_key .. ':timeout', timeout)
		redis.call('sadd', queue .. ':state:delayed', retrieved.id)
	end
end

-- move job to done queue if there's nothing more to be done
if 'done' == job.status or 'failed' == job.status then
	redis.call('rpush', queue .. ':state:done', job.id)

	-- update individual job stats
	retrieved.status = job.status
	retrieved.finished_at = job.timestamp
	redis.call(
		'hmset',
		job_key,
		-- fields
		'status',	job.status,
		'finished_at', job.timestamp
	)
end

-- publish event so clients can subscribe
redis.call('publish', queue .. ':job:' .. job.status, cjson.encode(retrieved))

-- from docs: Lua table with a single ok field -> Redis status reply
return { ok = true }