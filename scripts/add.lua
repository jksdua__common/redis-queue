-- constants
local PRIORITIES = { high = 1, normal = 1, low = 1 }
local DEFAULT_STATUS = 'queued'
local DEFAULT_TIMEOUT = 600 -- 10 minutes
local DEFAULT_BACKOFF = 0.5 -- half the timeout value from last attempt
local DEFAULT_ATTEMPTS = 0
local DEFAULT_PRIORITY = 'normal'
local DEFAULT_MAX_ATTEMPTS = 3

-- queue name
local queue = ARGV[1]
assert(queue, 'Missing queue')

-- job details
local job = nil
if not pcall(function() job = cjson.decode(ARGV[2]) end) then
	assert(job, 'Invalid job - expected json')
end
assert(job.id, 'Missing job id')
assert(job.data, 'Missing job data')

-- ensure job with the same id doesnt already exist
local existing = redis.call('hmget', queue .. ':job:' .. job.id, 'status')
assert(existing, 'Job with the same id already exists')

-- defaults
job.max_attempts = job.max_attempts or DEFAULT_MAX_ATTEMPTS
job.attempts = job.attempts or DEFAULT_ATTEMPTS
job.timeout = job.timeout or DEFAULT_TIMEOUT
job.backoff = job.backoff or DEFAULT_BACKOFF
job.status = job.status or DEFAULT_STATUS

job.priority = job.priority or DEFAULT_PRIORITY
assert(PRIORITIES[job.priority], 'Invalid job priority')

-- push job to end of queue
local queue_list = queue .. ':state:queued:priority:' .. job.priority
redis.call('rpush', queue_list, job.id)

redis.call(
	'hmset', queue .. ':job:' .. job.id,
	'data', job.data,
	'status', job.status,
	'timeout', job.timeout,
	'backoff', job.backoff,
	'priority', job.priority,
	'attempts', job.attempts,
	'max_attempts', job.max_attempts
)

return cjson.encode(job)