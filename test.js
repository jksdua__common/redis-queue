/* globals describe, before, beforeEach, it */

'use strict';

describe('#redis-queue', function() {
	var QUEUE = 'test';

	var _ = require('lodash');
	var chai = require('chai');
	var redis = require('redis');
	var assert = require('assert');
	var expect = chai.expect;
	var client = redis.createClient();
	var queue = require('./index')(client);

	var subscriber = redis.createClient();

	function flushdb(done) {
		client.flushdb(done);
	}

	before(function(done) {
		subscriber.psubscribe('*');
		subscriber.on('psubscribe', function() {
			done();
		});
	});

	describe('#event', function() {
		it('should work', function() {
			expect(
				queue.event(QUEUE, queue.constants.status.DONE)
			).to.equal('test:job:done');
		});
	});

	describe('#add', function() {
		// cleanup
		beforeEach(flushdb);

		it('should fail if job is not json', function(done) {
			queue.add(QUEUE, 'wateva', function(err) {
				expect(err).to.exist; // jshint ignore:line
				done();
			});
		});

		it('should fail if job is missing data', function(done) {
			queue.add(QUEUE, {}, function(err) {
				expect(err).to.match(/data/);
				done();
			});
		});

		it('should queue the job', function(done) {
			queue.add(QUEUE, { data: 'data' }, function(err, job) {
				expect(err).to.not.exist; // jshint ignore:line
				expect(job).to.have.property('id').that.is.a('string');
				expect(job).to.eql({
					id: job.id,
					attempts: 0,
					backoff: 0.5,
					data: 'data',
					max_attempts: 3,
					priority: 'normal',
					status: 'queued',
					timeout: 600
				});
				done();
			});
		});
	});

	describe('#process', function() {
		// cleanup
		beforeEach(flushdb);

		it('should return nothing if no jobs exist', function(done) {
			queue.process(QUEUE, function(err, jobs) {
				expect(err).to.not.exist; // jshint ignore:line
				expect(jobs).to.eql(void 0); // jshint ignore:line

				queue.process(QUEUE, 2, function(err, jobs) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(jobs).to.eql([]); // jshint ignore:line
					done();
				});
			});
		});

		it('should return job if it exists', function(done) {
			queue.add(QUEUE, { data: 'data' }, function(err, addedJob) {
				expect(err).to.not.exist; // jshint ignore:line

				queue.process(QUEUE, 3, function(err, jobs) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(jobs).to.eql([_.defaults({
						status: 'processing',
						attempts: 1
					}, addedJob)]);
					done();
				});
			});
		});

		it('should return higher priority task first', function(done) {
			queue.add(QUEUE, { data: 'data1' }, function(err, addedJob1) {
				expect(err).to.not.exist; // jshint ignore:line

				queue.add(QUEUE, { data: 'data2', priority: 'high' }, function(err, addedJob2) {
					expect(err).to.not.exist; // jshint ignore:line

					queue.process(QUEUE, function(err, job) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(job).to.not.eql(_.defaults({
							status: 'processing',
							attempts: 1
						}, addedJob1));
						expect(job).to.eql(_.defaults({
							status: 'processing',
							attempts: 1
						}, addedJob2));
						done();
					});
				});
			});
		});

		it('should return multiple jobs', function(done) {
			queue.add(QUEUE, { data: 'data' }, function(err, addedJob1) {
				expect(err).to.not.exist; // jshint ignore:line

				queue.add(QUEUE, { data: 'data', priority: 'high' }, function(err, addedJob2) {
					expect(err).to.not.exist; // jshint ignore:line

					queue.process(QUEUE, 7, function(err, jobs) {
						expect(err).to.not.exist; // jshint ignore:line
						expect(jobs).to.eql([
							_.defaults({
								status: 'processing',
								attempts: 1
							}, addedJob2),
							_.defaults({
								status: 'processing',
								attempts: 1
							}, addedJob1)
						]);
						done();
					});
				});
			});
		});

		it('should return a single job if multiple are not available', function(done) {
			queue.add(QUEUE, { data: 'data' }, function(err, addedJob) {
				expect(err).to.not.exist; // jshint ignore:line

				queue.process(QUEUE, 7, function(err, jobs) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(jobs).to.eql([_.defaults({
						status: 'processing',
						attempts: 1
					}, addedJob)]);
					done();
				});
			});
		});

		it('should return nothing if there are no more jobs to process', function(done) {
			queue.add(QUEUE, { data: 'data' }, function(err, job) {
				expect(err).to.not.exist; // jshint ignore:line

				queue.process(QUEUE, function(err) {
					expect(err).to.not.exist; // jshint ignore:line

					queue.status(QUEUE, {
						id: job.id,
						status: 'done',
						timestamp: Date.now()
					}, function(err) {
						expect(err).to.not.exist; // jshint ignore:line

						queue.process(QUEUE, function(err, job) {
							expect(err).to.not.exist; // jshint ignore:line
							expect(job).to.eql(void 0);
							done();
						});
					});
				});
			});
		});
	});

	describe('#status', function() {
		var job;

		// cleanup
		beforeEach(flushdb);

		beforeEach(function(done) {
			queue.add(QUEUE, { data: 'data' }, function(err) {
				expect(err).to.not.exist; // jshint ignore:line

				queue.process(QUEUE, function(err, j) {
					expect(err).to.not.exist; // jshint ignore:line
					job = j;
					done();
				});
			});
		});

		describe('#failed', function() {
			it('should fail if error message is not given', function(done) {
				queue.status(QUEUE, {
					id: job.id,
					status: queue.constants.status.FAILED,
					timestamp: Date.now()
				}, function(err) {
					expect(err.message).to.contain('Missing error');
					done(); 
				});
			});

			it('should work', function(done) {
				queue.status(QUEUE, {
					id: job.id,
					error: 'Some error',
					status: queue.constants.status.FAILED,
					timestamp: Date.now()
				}, done);
			});

			it('should emit failed attempt event', function(done) {
				function handler(pattern, channel, message) {
					var event = queue.event(QUEUE, queue.constants.status.FAILED_ATTEMPT);
					expect(channel).to.equal(event);
					expect(message).to.contain('id');
					subscriber.removeListener('pmessage', handler);
					done();
				}

				subscriber.on('pmessage', handler);

				queue.status(QUEUE, {
					id: job.id,
					error: 'Some error',
					status: queue.constants.status.FAILED,
					timestamp: Date.now()
				}, assert.ifError);
			});

			it('should emit failed event', function(done) {
				function handler(pattern, channel, message) {
					var event = queue.event(QUEUE, queue.constants.status.FAILED);
					expect(channel).to.equal(event);
					expect(message).to.contain('id');
					subscriber.removeListener('pmessage', handler);
					done();
				}

				subscriber.on('pmessage', handler);

				// hack in one attempt
				client.hmset(QUEUE + ':job:' + job.id, {
					max_attempts: 1
				}, function(err) {
					expect(err).to.not.exist; // jshint ignore:line
					queue.status(QUEUE, {
						id: job.id,
						error: 'Some error',
						status: queue.constants.status.FAILED,
						timestamp: Date.now()
					}, assert.ifError);
				});
			});
		});

		describe('#done', function() {
			it('should work', function(done) {
				queue.status(QUEUE, {
					id: job.id,
					status: queue.constants.status.DONE,
					timestamp: Date.now()
				}, done);
			});

			it('should emit an event', function(done) {
				function handler(pattern, channel, message) {
					var event = queue.event(QUEUE, queue.constants.status.DONE);
					expect(channel).to.equal(event);
					expect(message).to.contain('id');
					subscriber.removeListener('pmessage', handler);
					done();
				}

				subscriber.on('pmessage', handler);

				queue.status(QUEUE, {
					id: job.id,
					status: queue.constants.status.DONE,
					timestamp: Date.now()
				}, assert.ifError);
			});
		});
	});

	describe('#maintenance', function() {
		// cleanup
		beforeEach(flushdb);

		it('should requeue delayed tasks', function(done) {
			queue.add(QUEUE, { data: 'data', backoff: 0 }, function(err, job) {
				expect(err).to.not.exist; // jshint ignore:line
				queue.process(QUEUE, function(err) {
					expect(err).to.not.exist; // jshint ignore:line
					queue.status(QUEUE, {
						id: job.id,
						error: 'Some error',
						status: queue.constants.status.FAILED,
						timestamp: Date.now()
					}, function(err) {
						expect(err).to.not.exist; // jshint ignore:line

						queue.maintenance(QUEUE, Date.now(), function(err) {
							expect(err).to.not.exist; // jshint ignore:line

							queue.process(QUEUE, function(err, job2) {
								expect(err).to.not.exist; // jshint ignore:line
								expect(job2.id).to.equal(job.id);
								done();
							});
						});
					});
				});
			});
		});

		it('should requeue timed out tasks', function(done) {
			queue.add(QUEUE, { data: 'data', timeout: 0 }, function(err, job) {
				expect(err).to.not.exist; // jshint ignore:line
				queue.process(QUEUE, function(err) {
					expect(err).to.not.exist; // jshint ignore:line
					queue.maintenance(QUEUE, Date.now(), function(err) {
						expect(err).to.not.exist; // jshint ignore:line
						queue.process(QUEUE, function(err, job2) {
							expect(err).to.not.exist; // jshint ignore:line
							expect(job2.id).to.equal(job.id);
							done();
						});
					});
				});
			});
		});

		it('should remove completed jobs', function(done) {
			queue.add(QUEUE, { data: 'data' }, function(err, job) {
				expect(err).to.not.exist; // jshint ignore:line
				queue.process(QUEUE, function(err) {
					expect(err).to.not.exist; // jshint ignore:line
					queue.status(QUEUE, {
						id: job.id,
						status: queue.constants.status.DONE,
						timestamp: Date.now()
					}, function(err) {
						expect(err).to.not.exist; // jshint ignore:line
						queue.maintenance(QUEUE, Date.now(), function(err) {
							expect(err).to.not.exist; // jshint ignore:line
							client.keys('*', function(err, keys) {
								expect(err).to.not.exist; // jshint ignore:line
								expect(keys).to.have.length(0);
								done();
							});
						});
					});
				});
			});
		});
	});

	describe('#bugs', function() {
		it('should support object data', function(done) {
			queue.add(QUEUE, { data: { a: 'a', b: 'b' } }, function(err) {
				expect(err).to.not.exist; // jshint ignore:line
				queue.process(QUEUE, function(err, job) {
					expect(err).to.not.exist; // jshint ignore:line
					expect(job.data).to.eql({ a: 'a', b: 'b' });
					done();
				});
			});
		});
	});
});