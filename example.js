/* jshint node:true */

'use strict';

var QUEUE = 'example';

var redis = require('redis');
var assert = require('assert');

var client = redis.createClient();
var pubsub = redis.createClient();

// queue system
var queue = require('./index')(client);

// subscribe to all queue events
pubsub.psubscribe(QUEUE + ':job:*');

pubsub.on('pmessage', function(pattern, channel, message) {
	console.info('Received ' + channel + ' event with message: ');
	console.info(JSON.parse(message));
});

// subscribe to only completed jobs
var completedJobs = QUEUE + ':job:done';
pubsub.subscribe(completedJobs);
pubsub.on('message', function(channel, message) {
	console.info('Received a completed job event:');
	console.info(JSON.parse(message));
});

// perform maintenance every 10 seconds
	// requeue delayed tasks, clean up completed tasks
var interval = 1000 * 10; // 10 seconds
var completedRecordsToKeep = 100; // 100 jobs
setInterval(function() {
	console.info('Running maintenance tasks');
	queue.maintenance(QUEUE, Date.now(), completedRecordsToKeep, assert.ifError);
}, interval);


// === simulation ===

// simulate some tasks
console.info('Queuing some tasks in `' + QUEUE + '` queue');
queue.add(QUEUE, { data: 'job data 1', timeout: 5 }, assert.ifError);
queue.add(QUEUE, { data: 'job data 2', timeout: 5 }, assert.ifError);
queue.add(QUEUE, { data: 'job data 3', timeout: 5 }, assert.ifError);
queue.add(QUEUE, { data: 'job data 4', timeout: 5 }, assert.ifError);

// simulated workers
setTimeout(function() {
	queue.process(QUEUE, function(err, jobs) {
		assert.ifError(err);
		console.info('Processing job #1: ');
		console.info(jobs[0]);

		// simulate job done
		queue.status(QUEUE, {
			id: jobs[0].id,
			status: 'done',
			timestamp: Date.now()
		}, assert.ifError);
	});
}, 50);
setTimeout(function() {
	queue.process(QUEUE, function(err, jobs) {
		assert.ifError(err);
		console.info('Processing job #2: ');
		console.info(jobs[0]);

		// simulate job failed
		queue.status(QUEUE, {
			id: jobs[0].id,
			error: new Error('Failed to do bla'),
			status: 'failed',
			timestamp: Date.now()
		}, assert.ifError);
	});
}, 100);
setTimeout(function() {
	// expect job timeout
	queue.process(QUEUE, function(err, jobs) {
		assert.ifError(err);
		console.info('Processing job #2: ');
		console.info(jobs[0]);
	});
}, 150);