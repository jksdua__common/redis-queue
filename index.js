'use strict';

var fs = require('fs');
var crypto = require('crypto');

var SCRIPTS_DIR = __dirname + '/scripts';

var ADD = fs.readFileSync(SCRIPTS_DIR + '/add.lua').toString();
var STATUS = fs.readFileSync(SCRIPTS_DIR + '/status.lua').toString();
var PROCESS = fs.readFileSync(SCRIPTS_DIR + '/process.lua').toString();
var MAINTENANCE = fs.readFileSync(SCRIPTS_DIR + '/maintenance.lua').toString();

function jobId(done) {
	crypto.pseudoRandomBytes(16, function(err, id) {
		if (!err) { id = id.toString('hex'); }
		done(err, id);
	});
}

// creates a queue client
	// assumes underlying driver optimistically uses evalsha - this is the case for node_redis
function client(redisClient) {
	return {
		constants: {
			status: { DONE: 'done', FAILED: 'failed', QUEUED: 'queued', PROCESSING: 'processing', FAILED_ATTEMPT: 'failed_attempt' },
			priority: { HIGH: 'high', NORMAL: 'normal', LOW: 'low' }
		},
		event: function(queue, status) {
			return queue + ':job:' + status;
		},
		add: function(queue, job, done) {
			// add id to each job
			jobId(function(err, id) {
				if (err) { return done(err); }

				job.id = id;
				// bug - add support for object data
				job.data = JSON.stringify(job.data);
				job = JSON.stringify(job);
				// stringify job
				redisClient.eval([ADD, 0, queue, job], function(err, job) { // jshint ignore:line
					if (job) {
						job = JSON.parse(job);
						// bug - add support for object data
						job.data = JSON.parse(job.data);
					}
					done(err, job);
				});
			});
		},
		process: function(queue, number, done) {
			var args = [PROCESS, 0, queue, number];

			if (arguments.length === 2) {
				done = number;
				number = 1;
				args.pop();
			}

			redisClient.eval(args, function(err, jobs) { // jshint ignore:line
				if (err) { return done(err); }

				// remove null
				jobs.shift();
				// json parse jobs
				jobs = jobs.map(function(job) {
					job = JSON.parse(job);
					// bug - add support for object data
					job.data = JSON.parse(job.data);
					return job;
				});

				if (number === 1) {
					jobs = jobs[0];
				}

				done(err, jobs);
			});
		},
		status: function(queue, job, done) {
			job = JSON.stringify(job);
			redisClient.eval([STATUS, 0, queue, job], done); // jshint ignore:line
		},
		maintenance: function(queue, timestamp, recordsToKeep, done) {
			var args = [MAINTENANCE, 0, queue, timestamp, recordsToKeep];

			if (arguments.length === 3) {
				done = recordsToKeep;
				args.pop();
			}

			redisClient.eval(args, done); // jshint ignore:line
		}
	};
}

module.exports = exports = client;