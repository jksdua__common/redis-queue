Redis Queue
===========


Installation
------------

```
$ npm install --save git+ssh://git@gitlab.com:jksdua__common/redis-queue.git#v0.0.3
```


Usage
-----

### Client

```js
var client = require('redis').createClient();
var queue = require('js_redis-queue')(client);

var QUEUE = 'my awesome queue';

// simple
queue.add(QUEUE, { data: 'data' }, done);

// advanced
queue.add(QUEUE, {
	data: { a: 'a', b: 'b' },
	timeout: 60, // 1 minute
	backoff: 0.5, // exponential
	priority: queue.constants.priority.HIGH,
	max_attempts: 3
}, done);
```

### Worker

```js
var client = require('redis').createClient();
var queue = require('js_redis-queue')(client);

var QUEUE = 'my awesome queue';

// in-process worker pseudo code
function process() {
	// fetch job
		// on done, get new job
		// if no more jobs, loop every few seconds/minutes
}

// remote worker
var app = express();
app.get('/job/next', queue.process);
app.put('/job/:id', queue.status);
```

#### Maintenance worker

A maintenance worker cleans up the queue removing completed jobs, re-queueing timed out tasks etc.

```js
var assert = require('assert');
var client = require('redis').createClient();
var queue = require('js_redis-queue')(client);

var QUEUE = 'my awesome queue';

// run maintenance every 30 seconds
setInterval(function() {
	queue.maintenance(QUEUE, Date.now(), assert.ifError);
}, 30000);
```

Changelog
---------

### v0.0.3 (16 Sep 2014)
- Fixed bug where object data causes an error

### v0.0.2 (16 Sep 2014)
- Removed uuid
- Fixed bug in one of the tests

### v0.0.1 (14 Sep 2014)
- Initial commit